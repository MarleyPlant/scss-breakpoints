# scss-breakpoints
SCSS Library for implementing media queries as a mixin

## Getting started
creating a breakpoint rule is easy.
```css
@include for-size(tablet-landscape-up) {
    flex-direction: row;
}
```

where `tablet-landscape-up` is you can put any of the following breakpoints
- [x] phone-only (up to 600px)
- [x] tablet-portrait-up (above 600px)
- [x] tablet-landscape-up (above 900px)
- [x] desktop-up (above 1200px)
- [x] big-desktop-up (above 1800px)
